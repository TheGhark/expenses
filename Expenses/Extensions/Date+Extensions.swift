//
//  Date+Extensions.swift
//  Expenses
//
//  Created by Camilo Gaviria on 06/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

import Foundation

extension DateFormatter {
    static var standard: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        return dateFormatter
    }

    static var readable: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        dateFormatter.dateStyle = .medium
        return dateFormatter
    }
}

extension Date {
    init?(_ dateString: String) {
        guard let date = DateFormatter.standard.date(from: dateString) else {
            return nil
        }

        self.init(timeInterval: 0, since: date)
    }

    var currentTimeStamp: String? {
        let strTime = DateFormatter.standard.string(from: self)

        guard let convertedDate = DateFormatter.standard.date(from: strTime) else {
            return nil
        }

        return "\(Int64(convertedDate.timeIntervalSince1970))"
    }
}
