//
//  NumberFormatter.swift
//  Expenses
//
//  Created by Camilo Gaviria on 12/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

import Foundation

extension NumberFormatter {
    static var standard: NumberFormatter {
        return NumberFormatter()
    }

    static var currency: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.currencySymbol = Locale.current.currencySymbol
        formatter.numberStyle = .currency
        return formatter
    }
}
