//
//  AppDelegate.swift
//  Expenses
//
//  Created by Camilo Gaviria on 05/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

import CoreData
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var coreDataManager: CoreDataManager!

    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        coreDataManager = CoreDataManager()
        coreDataManager.initialize()
        return true
    }

    func applicationDidEnterBackground(_: UIApplication) {
        coreDataManager.saveContext()
    }
}
