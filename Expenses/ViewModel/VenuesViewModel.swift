//
//  VenuesViewModel.swift
//  Expenses
//
//  Created by Camilo Gaviria on 09/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

import CoreData
import Foundation

enum VenuesError: Error {
    case cantPerfom
}

protocol VenuesViewModelDelegate: class {
    func viewModelWillChange()
    func viewModelDidChange()
    func viewModelDidChange(type: NSFetchedResultsChangeType, at indexPath: IndexPath?, newIndexPath: IndexPath?)
    func viewModelDidFailed(with error: VenuesError)
}

@objc class VenuesViewModel: NSObject {
    typealias ChangeHandler = () -> Void
    typealias DidChangeObjectHandler = (NSFetchedResultsChangeType, IndexPath?, IndexPath?) -> Void
    typealias ErrorHandler = (VenuesError) -> Void

    private var context: NSManagedObjectContext!
    private var storageManager: StorageManager!
    private let priceFormatter = NumberFormatter.currency

    weak var delegate: VenuesViewModelDelegate?

    fileprivate lazy var fetchedResultsController: NSFetchedResultsController<Venue> = {
        let fetchRequest: NSFetchRequest<Venue> = Venue.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: false)]
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: context,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
        fetchedResultsController.delegate = self
        return fetchedResultsController
    }()

    var isEmpty: Bool {
        guard let fetchedObjects = fetchedResultsController.fetchedObjects else {
            return true
        }

        return fetchedObjects.isEmpty == true
    }

    var venues: [Venue] {
        return fetchedResultsController.fetchedObjects ?? []
    }

    var title: String {
        let now = Date()
        let year = Calendar.current.component(.year, from: now)
        let month = Calendar.current.component(.month, from: now)
        let monthName = DateFormatter.standard.monthSymbols[month - 1]
        return "\(monthName) \(year)"
    }

    init(storageManager: StorageManager? = nil,
         context: NSManagedObjectContext? = nil,
         delegate: VenuesViewModelDelegate? = nil) {
        self.storageManager = storageManager ?? StorageManager()
        self.delegate = delegate
        super.init()
        self.context = context ?? self.storageManager.persistentContainer.viewContext

        fetchedResultsController.delegate = self
        priceFormatter.currencySymbol = Locale.current.currencySymbol
        priceFormatter.numberStyle = .currency
    }

    func fetch() {
        do {
            try fetchedResultsController.performFetch()
        } catch {
            delegate?.viewModelDidFailed(with: .cantPerfom)
        }
    }

    func venue(at indexPath: IndexPath) -> Venue? {
        guard fetchedResultsController.fetchedObjects != nil else {
            return nil
        }

        return fetchedResultsController.object(at: indexPath)
    }

    func remove(venue: Venue, context: NSManagedObjectContext? = nil) {
        storageManager.remove(venue: venue, context: context)
        storageManager.save()
    }

    func amount(at indexPath: IndexPath) -> String? {
        guard let venue = venue(at: indexPath) else {
            return nil
        }

        let transactions = storageManager.fetchTransactions(venue: venue)
        var value: Double = 0.0

        for transaction in transactions {
            value += transaction.value
        }

        return priceFormatter.string(from: NSNumber(value: value))
    }
}

extension VenuesViewModel: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.viewModelWillChange()
    }

    func controller(_: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange _: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        delegate?.viewModelDidChange(type: type, at: indexPath, newIndexPath: newIndexPath)
    }

    func controllerDidChangeContent(_: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.viewModelDidChange()
    }
}
