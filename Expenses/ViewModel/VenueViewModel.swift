//
//  VenueViewModel.swift
//  Expenses
//
//  Created by Camilo Gaviria on 12/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

import CoreData
import Foundation

enum VenueError: Error {
    case cantPerfom
}

protocol VenueViewModelDelegate: class {
    func viewModelWillChange()
    func viewModelDidChange()
    func viewModelDidChange(type: NSFetchedResultsChangeType, at indexPath: IndexPath?, newIndexPath: IndexPath?)
    func viewModelDidFailed(with error: VenueError)
}

@objc class VenueViewModel: NSObject {
    private var context: NSManagedObjectContext!
    private var storageManager: StorageManager!
    private let priceFormatter = NumberFormatter.currency

    var venue: Venue! {
        didSet {
            fetch()
        }
    }

    weak var delegate: VenueViewModelDelegate?

    fileprivate lazy var fetchedResultsController: NSFetchedResultsController<Transaction> = {
        let fetchRequest: NSFetchRequest<Transaction> = Transaction.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        fetchRequest.predicate = NSPredicate(format: "venue.name == %@", venue.name ?? "")
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: context,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
        fetchedResultsController.delegate = self
        return fetchedResultsController
    }()

    var transactions: [Transaction] {
        return fetchedResultsController.fetchedObjects ?? []
    }

    init(storageManager: StorageManager? = nil,
         context: NSManagedObjectContext? = nil,
         delegate: VenueViewModelDelegate? = nil) {
        self.storageManager = storageManager ?? StorageManager()
        self.delegate = delegate
        super.init()
        self.context = context ?? self.storageManager.persistentContainer.viewContext
    }

    func transaction(at indexPath: IndexPath) -> Transaction? {
        let transformedIndexPath = IndexPath(row: indexPath.row, section: 0)

        guard transformedIndexPath.row < transactions.count else {
            return nil
        }

        return fetchedResultsController.object(at: transformedIndexPath)
    }

    func formattedValue(for transaction: Transaction) -> String? {
        return NumberFormatter.currency.string(from: NSNumber(value: transaction.value))
    }

    func fetch() {
        do {
            try fetchedResultsController.performFetch()
        } catch {
            delegate?.viewModelDidFailed(with: .cantPerfom)
        }
    }
}

extension VenueViewModel: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.viewModelWillChange()
    }

    func controller(_: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange _: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        delegate?.viewModelDidChange(type: type, at: indexPath, newIndexPath: newIndexPath)
    }

    func controllerDidChangeContent(_: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.viewModelDidChange()
    }
}
