//
//  NewTransactionViewModel.swift
//  Expenses
//
//  Created by Camilo Gaviria on 06/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

import CoreData
import Foundation

enum TransactionType: String {
    case credit = "Credit"
    case debit = "Debit"

    static let allTypes = [credit, debit]
}

class NewTransactionViewModel {
    private(set) var venue: String?
    var type: TransactionType = .credit

    private var price: NSNumber?
    private var date = Date()

    private let dateFormatter = DateFormatter()
    private let priceFormatter = NumberFormatter.currency

    var formattedDate: String {
        return dateFormatter.string(from: date)
    }

    var formattedPrice: String {
        guard let price = self.price,
            let formattedPrice = priceFormatter.string(from: price) else {
            return ""
        }

        return formattedPrice
    }

    var unformattedPrice: String {
        guard !formattedPrice.isEmpty,
            let currencySymbol = Locale.current.currencySymbol else {
            return ""
        }

        return formattedPrice.components(separatedBy: CharacterSet(charactersIn: "\(currencySymbol)\(priceFormatter.groupingSeparator ?? "")")).joined()
    }

    init() {
        dateFormatter.dateStyle = .medium
    }

    func save(context: NSManagedObjectContext? = nil) {
        let storageManager = StorageManager()
        let workingContext = context ?? storageManager.persistentContainer.viewContext

        guard let venue = self.venue else {
            return
        }

        var foundVenue = storageManager.fetchVenue(name: venue, city: "", country: "", context: workingContext)

        if foundVenue == nil {
            foundVenue = storageManager.insertVenue(name: venue, city: "", country: "", context: workingContext)
        }

        guard
            let price = price,
            let transactionVenue = foundVenue
        else {
            return
        }

        storageManager.insertTransaction(value: price.doubleValue, date: date, type: type, venue: transactionVenue, context: workingContext)
        storageManager.save()
    }

    func selectType(at index: Int) {
        guard index < TransactionType.allTypes.count else {
            return
        }
        type = TransactionType.allTypes[index]
    }

    func update(date: Date) {
        self.date = date
    }

    func update(price text: String?) {
        guard let priceText = text,
            let price = NumberFormatter.standard.number(from: priceText)
        else {
            return
        }

        self.price = price
    }

    func update(venue: String?) {
        self.venue = venue
    }
}
