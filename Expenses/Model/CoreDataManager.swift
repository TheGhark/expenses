//
//  CoreDataManager.swift
//  Expenses
//
//  Created by Camilo Gaviria on 05/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

import CoreData
import Foundation

class CoreDataManager {
    let persistentContainer: NSPersistentContainer

    init(persistentContainer: NSPersistentContainer? = nil) {
        self.persistentContainer = persistentContainer ?? CoreDataManager.defaultContainer
    }

    private class var defaultContainer: NSPersistentContainer {
        let container = NSPersistentContainer(name: "Expenses")
        container.loadPersistentStores(completionHandler: { _, error in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }

    func initialize() {
        if persistentContainer.viewContext.hasChanges {
            do {
                try persistentContainer.viewContext.save()
            } catch {
                print("Could not initialize fakes: \(error)")
            }
        }
    }

    func saveContext() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    func flushData() {
        flushData(entity: "Currency")
        flushData(entity: "CurrencyPair")
    }

    func flushData(entity: String) {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

        do {
            try persistentContainer.viewContext.execute(deleteRequest)
            try persistentContainer.viewContext.save()
        } catch {
            print("Could not flush data: \(error)")
        }
    }
}
