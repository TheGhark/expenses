//
//  StorageManager.swift
//  Expenses
//
//  Created by Camilo Gaviria on 05/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

import CoreData
import Foundation
import UIKit

class StorageManager {
    let persistentContainer: NSPersistentContainer

    init(persistentContainer: NSPersistentContainer) {
        self.persistentContainer = persistentContainer
        self.persistentContainer.viewContext.automaticallyMergesChangesFromParent = true
    }

    convenience init() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError("Cannot get shared app delegate")
        }

        self.init(persistentContainer: appDelegate.coreDataManager.persistentContainer)
    }

    func save() {
        if persistentContainer.viewContext.hasChanges {
            do {
                try persistentContainer.viewContext.save()
            } catch {
                print("Could not save: \(error)")
            }
        }
    }

    @discardableResult
    public func insertVenue(name: String, city: String, country: String, context: NSManagedObjectContext? = nil) -> Venue? {
        let workingContext = context ?? persistentContainer.viewContext

        guard let newVenue: Venue = insert(context: workingContext) else {
            return nil
        }

        newVenue.setValue(name, forKey: "name")
        newVenue.setValue(city, forKey: "city")
        newVenue.setValue(country, forKey: "country")

        return newVenue
    }

    @discardableResult
    public func insertTransaction(value: Double,
                                  date: Date,
                                  type: TransactionType = .debit,
                                  venue: Venue,
                                  context: NSManagedObjectContext? = nil) -> Transaction? {
        let workingContext = context ?? persistentContainer.viewContext

        guard let newTransaction: Transaction = insert(context: workingContext) else {
            return nil
        }

        guard let vName = venue.name,
            let vCity = venue.city,
            let vCountry = venue.country,
            !vName.isEmpty else {
            return nil
        }

        let newVenue = fetchVenue(name: vName, city: vCity, country: vCountry, context: workingContext)

        newTransaction.setValue(value, forKey: "value")
        newTransaction.setValue(date.currentTimeStamp, forKey: "date")
        newTransaction.setValue(newVenue, forKey: "venue")
        newTransaction.setValue(type.rawValue, forKey: "type")

        return newTransaction
    }

    @discardableResult
    public func fetchTransactions(venue: Venue, context: NSManagedObjectContext? = nil) -> [Transaction] {
        let workingContext = context ?? persistentContainer.viewContext

        guard let vName = venue.name,
            let vCity = venue.city,
            let vCountry = venue.country,
            !vName.isEmpty else {
            return []
        }

        let fetchRequest: NSFetchRequest<Transaction> = NSFetchRequest(entityName: "Transaction")
        fetchRequest.predicate = NSPredicate(format: "venue.name == %@ && venue.city == %@ && venue.country == %@", vName, vCity, vCountry)

        do {
            let result = try workingContext.fetch(fetchRequest)
            return result
        } catch {
            print("Could not fetch venue: \(error)")
        }

        return []
    }

    @discardableResult
    public func fetchTransactions(from: Date, toDate: Date, context: NSManagedObjectContext? = nil) -> [Transaction] {
        let workingContext = context ?? persistentContainer.viewContext

        guard let fromTimeStamp = from.currentTimeStamp,
            let toTimeStamp = toDate.currentTimeStamp else {
            return []
        }

        let fetchRequest: NSFetchRequest<Transaction> = NSFetchRequest(entityName: "Transaction")
        fetchRequest.predicate = NSPredicate(format: "date >= %@ && date <= %@", fromTimeStamp, toTimeStamp)

        do {
            let result = try workingContext.fetch(fetchRequest)
            return result
        } catch {
            print("Could not fetch transactions from \(from) to \(toDate). Error:\(error)")
        }

        return []
    }

    @discardableResult
    public func fetchVenue(name: String, city: String, country: String, context: NSManagedObjectContext? = nil) -> Venue? {
        let workingContext = context ?? persistentContainer.viewContext

        let fetchRequest: NSFetchRequest<Venue> = NSFetchRequest(entityName: "Venue")
        fetchRequest.predicate = NSPredicate(format: "name == %@ && city == %@ && country == %@", name, city, country)

        do {
            let result = try workingContext.fetch(fetchRequest)
            return result.first
        } catch {
            print("Could not fetch venue: \(error)")
        }

        return nil
    }

    public func remove(venue: Venue, context: NSManagedObjectContext? = nil) {
        remove(objectID: venue.objectID, context: context)
    }

    public func remove(transaction: Transaction, context: NSManagedObjectContext? = nil) {
        remove(objectID: transaction.objectID, context: context)
    }

    @discardableResult
    public func fetchAllVenues(context: NSManagedObjectContext? = nil) -> [Venue] {
        let workingContext = context ?? persistentContainer.viewContext
        let request: NSFetchRequest<Venue> = Venue.fetchRequest()
        let results = try? workingContext.fetch(request)
        return results ?? []
    }

    private func insert<E: NSManagedObject>(context: NSManagedObjectContext) -> E? {
        guard let entity = NSEntityDescription.insertNewObject(forEntityName: String(describing: E.self), into: context) as? E else {
            return nil
        }

        return entity
    }

    private func remove(objectID: NSManagedObjectID, context: NSManagedObjectContext? = nil) {
        let workingContext = context ?? persistentContainer.viewContext
        let object = workingContext.object(with: objectID)
        workingContext.delete(object)
    }
}
