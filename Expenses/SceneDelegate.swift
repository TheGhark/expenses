//
//  SceneDelegate.swift
//  Expenses
//
//  Created by Camilo Gaviria on 05/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func sceneDidEnterBackground(_: UIScene) {
        (UIApplication.shared.delegate as? AppDelegate)?.coreDataManager.saveContext()
    }
}
