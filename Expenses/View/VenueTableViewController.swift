//
//  VenueTableViewController.swift
//  Expenses
//
//  Created by Camilo Gaviria on 12/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

import UIKit

class VenueTableViewController: UITableViewController {
    private let detailCellIdentifier = "DetailCellIdentifier"
    private let transactionCellIdentifier = "TransactionCellIdentifier"

    var venue: Venue {
        get { return viewModel.venue }
        set { viewModel.venue = newValue }
    }

    private let viewModel = VenueViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in _: UITableView) -> Int {
        return 2
    }

    override func tableView(_: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        }

        return viewModel.transactions.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: detailCellIdentifier, for: indexPath)

            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "Name"
                cell.detailTextLabel?.text = venue.name
            case 1:
                cell.textLabel?.text = "City"
                cell.detailTextLabel?.text = venue.city
            case 2:
                cell.textLabel?.text = "Country"
                cell.detailTextLabel?.text = venue.country
            default:
                cell.textLabel?.text = nil
                cell.detailTextLabel?.text = nil
            }

            return cell
        } else {
            guard let transaction = viewModel.transaction(at: indexPath) else {
                preconditionFailure("Impossilbe scenario. Expected transaction")
            }

            let cell = tableView.dequeueReusableCell(withIdentifier: transactionCellIdentifier, for: indexPath)
            cell.textLabel?.text = transaction.type ?? TransactionType.debit.rawValue
            cell.detailTextLabel?.text = viewModel.formattedValue(for: transaction)
            return cell
        }
    }

    override func tableView(_: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            return "Transactions"
        }

        return nil
    }
}
