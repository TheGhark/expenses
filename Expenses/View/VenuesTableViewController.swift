//
//  VenuesTableViewController.swift
//  Expenses
//
//  Created by Camilo Gaviria on 09/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

import CoreData
import UIKit

class VenuesTableViewController: UITableViewController {
    let viewModel = VenuesViewModel()
    let cellIdentifier = "TransactionCellidentifier"
    let venueSegueIdentifier = "VenueSegueIdentifier"

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefreshControler), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.fetch()
        title = viewModel.title
    }

    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return viewModel.venues.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        let venue = viewModel.venue(at: indexPath)
        cell.textLabel?.text = venue?.name
        cell.detailTextLabel?.text = viewModel.amount(at: indexPath)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: venueSegueIdentifier, sender: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == venueSegueIdentifier {
            guard
                let indexPath = sender as? IndexPath,
                let venue = viewModel.venue(at: indexPath) else {
                return
            }

            let venueViewController = segue.destination as? VenueTableViewController
            venueViewController?.venue = venue
            super.prepare(for: segue, sender: sender)
        }
    }

    @objc private func handleRefreshControler() {
        DispatchQueue.main.async {
            self.viewModel.fetch()
            self.tableView.refreshControl?.endRefreshing()
            self.tableView.reloadData()
        }
    }
}

extension VenuesTableViewController: VenuesViewModelDelegate {
    func viewModelWillChange() {
        tableView.beginUpdates()
    }

    func viewModelDidChange() {
        tableView.endUpdates()
    }

    func viewModelDidChange(type: NSFetchedResultsChangeType, at indexPath: IndexPath?, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .automatic)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        case .update:
            if let indexPath = indexPath {
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        case .move:
            if let indexPath = indexPath, let newIndexPath = newIndexPath {
                tableView.reloadRows(at: [indexPath, newIndexPath], with: .automatic)
            }
        default:
            print("Unsupported change type: \(type)")
        }
    }

    func viewModelDidFailed(with error: VenuesError) {
        let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        present(alertController, animated: true, completion: nil)
    }
}
