//
//  NewTransactionTableViewController.swift
//  Expenses
//
//  Created by Camilo Gaviria on 06/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

import UIKit

class NewTransactionTableViewController: UITableViewController {
    private let dateCellIdentifier = "DateCellIdentifier"
    private let datePickerCellIdentifier = "DatePickerCellIdentifier"
    private let viewModel = NewTransactionViewModel()
    private var isDatePickerVisible = false

    @IBOutlet var venueTextField: UITextField!
    @IBOutlet var priceTextField: UITextField!
    @IBOutlet var typeSegmentedControl: UISegmentedControl!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var segmentedControl: UISegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupAppeareance()
    }

    @IBAction func cancelButtonTapped(_: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func saveButtonTapped(_: UIBarButtonItem) {
        viewModel.save()
        dismiss(animated: true, completion: nil)
    }

    @IBAction func datePickerValueChanged(_ sender: UIDatePicker) {
        viewModel.update(date: sender.date)
        dateLabel.text = viewModel.formattedDate
    }

    @IBAction func priceEditingDidEnd(_ sender: UITextField) {
        viewModel.update(price: sender.text)
        sender.text = viewModel.formattedPrice
    }

    @IBAction func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        viewModel.selectType(at: sender.selectedSegmentIndex)
    }

    @IBAction func venueEditingDidEnd(_ sender: UITextField) {
        viewModel.update(venue: sender.text)
    }

    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return isDatePickerVisible ? 5 : 4
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)

        if cell?.reuseIdentifier == dateCellIdentifier {
            isDatePickerVisible = !isDatePickerVisible
            tableView.beginUpdates()

            if isDatePickerVisible {
                dateLabel.textColor = .red
                tableView.insertRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
            } else {
                dateLabel.textColor = .black
                tableView.deleteRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
            }

            tableView.endUpdates()
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }

    fileprivate func setupAppeareance() {
        tableView.tableFooterView = UIView()
        datePicker.maximumDate = Date()
        dateLabel.text = viewModel.formattedDate
        dateLabel.textColor = .black

        segmentedControl.removeAllSegments()
        for (index, type) in TransactionType.allTypes.enumerated() {
            segmentedControl.insertSegment(withTitle: type.rawValue, at: index, animated: false)
        }
        segmentedControl.selectedSegmentIndex = 0

        venueTextField.delegate = self
        priceTextField.delegate = self
    }

    @objc private func dismissKeyboard() {
        priceTextField.resignFirstResponder()
    }
}

extension NewTransactionTableViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == priceTextField {
            let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
            let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissKeyboard))
            toolbar.items = [flexSpace, doneButton]
            toolbar.sizeToFit()
            textField.inputAccessoryView = toolbar

            textField.text = viewModel.unformattedPrice
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn _: NSRange, replacementString string: String) -> Bool {
        guard !string.isEmpty else {
            return true
        }

        if textField == priceTextField {
            if let text = textField.text, !text.isEmpty {
                let number = NumberFormatter.standard.number(from: text + string)
                return number != nil
            }
        }

        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == venueTextField {
            priceTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }

        return true
    }
}
