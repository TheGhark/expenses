//
//  VenueTests.swift
//  ExpensesTests
//
//  Created by Camilo Gaviria on 06/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

@testable import Expenses
import XCTest

class VenueTests: BaseTestCase {
    override func tearDown() {
        super.tearDown()
        flushData(entity: "Transaction")
        flushData(entity: "Venue")
    }

    func testInsertVenue() {
        storageManager.insertVenue(name: "Carrefour", city: "Kraków", country: "Poland", context: mockPersistentContainer.viewContext)

        let venues = storageManager.fetchAllVenues()
        XCTAssertEqual(venues.count, 1)
    }

    func testDeleteVenue() {
        storageManager.insertVenue(name: "Carrefour", city: "Kraków", country: "Poland", context: mockPersistentContainer.viewContext)

        var venues = storageManager.fetchAllVenues()

        guard let venue = venues.first else {
            preconditionFailure("No venue found")
        }

        storageManager.remove(venue: venue, context: mockPersistentContainer.viewContext)
        venues = storageManager.fetchAllVenues()
        XCTAssertEqual(venues.count, 0)
    }

    func testFetchVenue() {
        storageManager.insertVenue(name: "Carrefour", city: "Kraków", country: "Poland", context: mockPersistentContainer.viewContext)

        let venue = storageManager.fetchVenue(name: "Carrefour", city: "Kraków", country: "Poland")
        XCTAssertNotNil(venue)
    }
}
