//
//  NewTransactionViewModelTests.swift
//  ExpensesTests
//
//  Created by Camilo Gaviria on 06/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

@testable import Expenses
import XCTest

class NewTransactionViewModelTests: BaseTestCase {
    var sut: NewTransactionViewModel!
    var dateFormatter = DateFormatter()

    override func setUp() {
        super.setUp()

        sut = NewTransactionViewModel()
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .medium
    }

    func testFormattedDateEmpty() {
        let now = Date()
        XCTAssertEqual(sut.formattedDate, dateFormatter.string(from: now))
    }

    func testFormattedDate() {
        guard let date = Date("2019-01-01") else {
            preconditionFailure("Cannot create date")
        }

        sut.update(date: date)
        XCTAssertEqual(sut.formattedDate, dateFormatter.string(from: date))
    }

    func testFormattedPriceEmpty() {
        XCTAssertEqual(sut.formattedPrice, "")
    }

    func testFormattedPrice() {
        let expected = NumberFormatter.currency.string(from: NSNumber(value: 45.45))
        sut.update(price: "45,45")
        XCTAssertEqual(sut.formattedPrice, expected)
    }

    func testUnformattedPriceEmpty() {
        XCTAssertEqual(sut.unformattedPrice, "")
    }

    func testUnformattedPrice() {
        sut.update(price: "45,45")
        XCTAssertEqual(sut.unformattedPrice, "45,45")
    }

    func testUnformattedPriceThousand() {
        sut.update(price: "4500,45")
        XCTAssertEqual(sut.unformattedPrice, "4500,45")
    }

    func testSave() {
        let name = "Carrefour"
        let now = Date()
        sut.update(venue: name)
        sut.update(price: "45,45")
        sut.update(date: now)
        sut.save(context: mockPersistentContainer.viewContext)

        guard let venue = storageManager.fetchVenue(name: name, city: "", country: "", context: mockPersistentContainer.viewContext) else {
            preconditionFailure("Could not find venue")
        }

        let transactions = storageManager.fetchTransactions(venue: venue, context: mockPersistentContainer.viewContext)
        let found = transactions.first { $0.date == now.currentTimeStamp && $0.value == 45.45 && $0.venue?.name == venue.name }
        XCTAssertNotNil(found)
    }

    func testSelectTypes() {
        sut.selectType(at: 0)
        XCTAssertEqual(sut.type, .credit)

        sut.selectType(at: 1)
        XCTAssertEqual(sut.type, .debit)

        sut.selectType(at: 2)
        XCTAssertEqual(sut.type, .debit)
    }

    func testUpdatePriceNil() {
        sut.update(price: nil)
        XCTAssertEqual(sut.unformattedPrice, "")
    }

    func testUpdatePrice() {
        let price = NumberFormatter.currency.string(from: NSNumber(value: 45.45))
        sut.update(price: "45,45")
        XCTAssertEqual(sut.formattedPrice, price)
    }

    func testUpdatePriceInvalid() {
        sut.update(price: "abcd")
        XCTAssertEqual(sut.formattedPrice, "")
    }

    func testUpdateVenueEmpty() {
        sut.update(venue: nil)
        XCTAssertNil(sut.venue)
    }

    func testUpdateVenue() {
        sut.update(venue: "Carrefour")
        XCTAssertEqual(sut.venue, "Carrefour")
    }
}
