//
//  TransactionsViewModelTests.swift
//  ExpensesTests
//
//  Created by Camilo Gaviria on 09/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

@testable import Expenses
import XCTest

class VenuesViewModelTests: BaseTestCase {
    var sut: VenuesViewModel!

    override func setUp() {
        super.setUp()
        sut = VenuesViewModel(storageManager: storageManager, context: mockPersistentContainer.viewContext)
    }

    override func tearDown() {
        super.tearDown()
    }

    func testIsEmpty() {
        XCTAssertTrue(sut.isEmpty)
    }

    func testIsEmpty_False() {
        initStubs()
        sut.fetch()
        XCTAssertFalse(sut.isEmpty)
    }

    func testVenues_Empty() {
        XCTAssertTrue(sut.venues.isEmpty)
    }

    func testVenues() {
        initStubs()
        sut.fetch()
        XCTAssertEqual(sut.venues.count, 1)
    }

    func testVenueAtIndexPath_NotFetched() {
        let transaction = sut.venue(at: IndexPath(row: 0, section: 0))
        XCTAssertNil(transaction)
    }

    func testVenueAtIndexPath() {
        initStubs()
        sut.fetch()
        let transaction = sut.venue(at: IndexPath(row: 0, section: 0))
        XCTAssertNotNil(transaction)
    }

    func testRemoveVenue() {
        initStubs()
        sut.fetch()
        let previousCount = sut.venues.count
        let venue = sut.venues[0]
        sut.remove(venue: venue, context: mockPersistentContainer.viewContext)
        sut.fetch()
        XCTAssertNotEqual(previousCount, sut.venues.count)
    }

    func testAmountAtIndexPath() {
        initStubs()
        sut.fetch()
        let amount = sut.amount(at: IndexPath(row: 0, section: 0))
        let expected = NumberFormatter.currency.string(from: NSNumber(value: 4000))
        XCTAssertEqual(amount, expected)
    }

    private func initStubs() {
        guard let venue = storageManager.insertVenue(name: "Carrefour", city: "Kraków", country: "Poland", context: mockPersistentContainer.viewContext) else {
            preconditionFailure("Could not create venue")
        }

        storageManager.insertTransaction(value: 1000, date: Date(), venue: venue, context: mockPersistentContainer.viewContext)
        storageManager.insertTransaction(value: 1000, date: Date(), venue: venue, context: mockPersistentContainer.viewContext)
        storageManager.insertTransaction(value: 1000, date: Date(), venue: venue, context: mockPersistentContainer.viewContext)
        storageManager.insertTransaction(value: 1000, date: Date(), venue: venue, context: mockPersistentContainer.viewContext)

        do {
            try mockPersistentContainer.viewContext.save()
        } catch {
            print("Could not initialize fakes: \(error)")
        }
    }
}
