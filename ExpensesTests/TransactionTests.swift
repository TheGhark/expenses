//
//  TransactionTests.swift
//  ExpensesTests
//
//  Created by Camilo Gaviria on 05/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

@testable import Expenses
import XCTest

class TransactionTests: BaseTestCase {
    override func setUp() {
        super.setUp()
        initStubs()
    }

    override func tearDown() {
        super.tearDown()
        flushData(entity: "Transaction")
    }

    func testCreateTransaction() {
        guard let venue = storageManager.fetchAllVenues(context: mockPersistentContainer.viewContext).first else {
            preconditionFailure("Could not find venue")
        }

        let transaction = storageManager.insertTransaction(value: 5000, date: Date(), venue: venue, context: mockPersistentContainer.viewContext)
        XCTAssertNotNil(transaction)
    }

    func testDeleteTransaction() {
        guard let venue = storageManager.fetchVenue(name: "Carrefour", city: "Kraków", country: "Poland") else {
            preconditionFailure("Could not find venue")
        }

        let previousTransactions = storageManager.fetchTransactions(venue: venue, context: mockPersistentContainer.viewContext)
        guard let transaction = previousTransactions.last else {
            preconditionFailure("Could not find expenses")
        }

        storageManager.remove(transaction: transaction, context: mockPersistentContainer.viewContext)
        storageManager.save()

        let currentCount = storageManager.fetchTransactions(venue: venue, context: mockPersistentContainer.viewContext).count
        XCTAssertTrue(previousTransactions.count > currentCount)
    }

    func testFetchTransactions() {
        guard let venue = storageManager.fetchVenue(name: "Carrefour", city: "Kraków", country: "Poland") else {
            preconditionFailure("Could not find venue")
        }

        let transactions = storageManager.fetchTransactions(venue: venue, context: mockPersistentContainer.viewContext)
        XCTAssertEqual(transactions.count, 4)
    }

    func testUpdateTransaction() {
        guard let venue = storageManager.fetchVenue(name: "Carrefour", city: "Kraków", country: "Poland") else {
            preconditionFailure("Could not find venue")
        }

        var transactions = storageManager.fetchTransactions(venue: venue, context: mockPersistentContainer.viewContext)
        guard let transaction = transactions.last else {
            preconditionFailure("Could not find expenses")
        }

        transaction.setValue(5000, forKey: "value")

        storageManager.save()

        transactions = storageManager.fetchTransactions(venue: venue, context: mockPersistentContainer.viewContext)
        let found = transactions.first { $0.value == 5000 }

        XCTAssertNotNil(found)
    }

    func testFetchWeekTransactions() {
        guard let venue = storageManager.fetchVenue(name: "Carrefour", city: "Kraków", country: "Poland") else {
            preconditionFailure("Could not find venue")
        }

        guard
            let startDate = Date("2019-01-01"),
            let endDate = Date("2019-01-07") else {
            preconditionFailure("Cannot create dates")
        }

        storageManager.insertTransaction(value: 1000, date: startDate, venue: venue, context: mockPersistentContainer.viewContext)
        storageManager.insertTransaction(value: 1000, date: startDate, venue: venue, context: mockPersistentContainer.viewContext)
        storageManager.insertTransaction(value: 1000, date: endDate, venue: venue, context: mockPersistentContainer.viewContext)
        storageManager.insertTransaction(value: 1000, date: endDate, venue: venue, context: mockPersistentContainer.viewContext)

        let transactions = storageManager.fetchTransactions(from: startDate, toDate: endDate, context: mockPersistentContainer.viewContext)
        XCTAssertEqual(transactions.count, 4)
    }

    private func initStubs() {
        guard let venue = storageManager.insertVenue(name: "Carrefour", city: "Kraków", country: "Poland", context: mockPersistentContainer.viewContext) else {
            preconditionFailure("Could not create venue")
        }

        storageManager.insertTransaction(value: 1000, date: Date(), venue: venue, context: mockPersistentContainer.viewContext)
        storageManager.insertTransaction(value: 1000, date: Date(), venue: venue, context: mockPersistentContainer.viewContext)
        storageManager.insertTransaction(value: 1000, date: Date(), venue: venue, context: mockPersistentContainer.viewContext)
        storageManager.insertTransaction(value: 1000, date: Date(), venue: venue, context: mockPersistentContainer.viewContext)

        do {
            try mockPersistentContainer.viewContext.save()
        } catch {
            print("Could not initialize fakes: \(error)")
        }
    }
}
