//
//  VenueViewModelTests.swift
//  ExpensesTests
//
//  Created by Camilo Gaviria on 12/12/2019.
//  Copyright © 2019 Heima. All rights reserved.
//

@testable import Expenses
import XCTest

class VenueViewModelTests: BaseTestCase {
    var sut: VenueViewModel!

    override func setUp() {
        super.setUp()
        sut = VenueViewModel(storageManager: storageManager, context: mockPersistentContainer.viewContext)
        initStubs()
    }

    override func tearDown() {
        super.tearDown()
        flushData(entity: "Transaction")
        flushData(entity: "Venue")
    }

    func testTransactionAtIndexPath() {
        let transaction = sut.transaction(at: IndexPath(row: 0, section: 0))
        XCTAssertNotNil(transaction)
    }

    func testTransactionAtIndexPath_Invalid() {
        let transaction = sut.transaction(at: IndexPath(row: 10, section: 0))
        XCTAssertNil(transaction)
    }

    func testFormattedValue() {
        guard let transaction = sut.transactions.first else {
            preconditionFailure("No transaction found")
        }
        let expected = NumberFormatter.currency.string(from: NSNumber(value: 1000))
        guard let formatted = sut.formattedValue(for: transaction) else {
            preconditionFailure("Expected value")
        }
        XCTAssertEqual(expected, formatted)
    }

    private func initStubs() {
        guard let venue = storageManager.insertVenue(name: "Carrefour", city: "Kraków", country: "Poland", context: mockPersistentContainer.viewContext) else {
            preconditionFailure("Could not create venue")
        }

        storageManager.insertTransaction(value: 1000, date: Date(), venue: venue, context: mockPersistentContainer.viewContext)
        storageManager.insertTransaction(value: 1000, date: Date(), venue: venue, context: mockPersistentContainer.viewContext)
        storageManager.insertTransaction(value: 1000, date: Date(), venue: venue, context: mockPersistentContainer.viewContext)
        storageManager.insertTransaction(value: 1000, date: Date(), venue: venue, context: mockPersistentContainer.viewContext)
        sut.venue = venue

        do {
            try mockPersistentContainer.viewContext.save()
        } catch {
            print("Could not initialize fakes: \(error)")
        }
    }
}
